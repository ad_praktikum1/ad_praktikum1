
package model;

import setPackage.Set_3;

/**
 * Created by Ronja on 03.10.2017.
 */

public class Knoten<T> {
    Elem<T> dat;
    Knoten<T> next;
    Position pos;
    int nextIndex;
    int prevIndex;

    public Knoten(Elem<T> d){
    dat= d;
    }
    
    public void setPosition(Position posi) {
    	this.pos=posi;
    }
    public Position getPosition() {
    	return pos;
    }
    public void setNext(Knoten<T> knoten) {
    next=knoten;
    }
    public Knoten<T>getNext(){
        return next;
    }
    public void setElem(Elem d){
    	dat=d;
    }
    public Elem getElem(){

        return dat;
    }
    public void setNextIndex(int index) {
    	this.nextIndex=index;
    }
    public void setPrevIndex(int index) {
    	this.prevIndex=index;
    }
    public int getNextIndex() {
    	return nextIndex;
    }
    public int getPrevIndex() {
    	return prevIndex;
    }
}


