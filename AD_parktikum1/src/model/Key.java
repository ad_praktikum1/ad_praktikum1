package model;

public class Key {
	private int key;
	
	public Key(int key) {
		this.key = key;
	}

	public int getKey() {
		return key;
	}

	@Override
	public int hashCode() {
		return key;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Key other = (Key) obj;
		if (key != other.key)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Key [key=" + key + "]";
	}
	
	
	
}
