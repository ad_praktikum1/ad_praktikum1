package model;

import setPackage.Set_I;

public class Position {
	private boolean isValid;
	private int pointer;
	private Set_I set;
	private Knoten knoten; 
	
	public Position(boolean isValid, int pointer, Set_I set, Knoten knoten) {
		this.isValid = isValid;
		this.pointer = pointer;
		this.set = set;
		this.knoten = knoten;
	}
	public Position(boolean isValid, int pointer, Set_I set) {
		this.isValid = isValid;
		this.pointer = pointer;
		this.set = set;
	}
	
	public Position( int pointer, Set_I set) {
		this.isValid = true;
		this.pointer = pointer;
		this.set = set;
	}
	
	public void setKnoten(Knoten knt) {
		knoten = knt;
	}
	
	public Knoten getKnoten() {
		return knoten;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public int getPointer() {
		return pointer;
	}

	public void setPointer(int pointer) {
		this.pointer = pointer;
	}

	public Set_I getSet() {
		return set;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isValid ? 1231 : 1237);
		result = prime * result + pointer;
		result = prime * result + ((set == null) ? 0 : set.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (isValid != other.isValid)
			return false;
		if (pointer != other.pointer)
			return false;
		if (set == null) {
			if (other.set != null)
				return false;
		} else if (!set.equals(other.set))
			return false;
		return true;
	}
	
	
	
	
}
