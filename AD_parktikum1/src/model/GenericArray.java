package model;

import java.util.Arrays;

public class GenericArray<L> {
	private L[] array;
	int length;
	
	public GenericArray(int size) {
		this.length = size;
		array = (L[]) new Object[size];
	}
	
	public L get(int index) {
		return array[index];
	}
	
	public void set (int index, L l) {
		array[index] = l;
	}

	public int getLength() {
		return length;
	}

	@Override
	public String toString() {
		return "GenericArray [array=" + Arrays.toString(array) + ", length=" + length + "]";
	}
	
}
