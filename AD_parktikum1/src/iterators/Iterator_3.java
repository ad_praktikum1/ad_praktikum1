package iterators;

import java.util.Iterator;

import model.Knoten;
import setPackage.Set_1;
import setPackage.Set_3;


// E: DataType
// T: Elem<DataType>
public class Iterator_3<T, E> implements Iterator<T> {

	private Set_3<E> set;
	int pointer;
	Knoten knoten;
	
	
	public Iterator_3(Set_3<E> set) {
		this.set = set;
		this.pointer = 0;
		this.knoten = set.getHead();
		
	}

	
	@Override
	public boolean hasNext() {
		if(knoten.getNext().getPosition().getPointer() <set.getTail().getPosition().getPointer()) {
			return true;
		}
		return false;
		/*
		if(set.getHead().getPosition().getPointer() < set.size()) {
			return true;
		}
		return false;
		*/
	}

	@Override
	public T next() {
		pointer++;
		//System.out.println(pointer);
		//System.out.println((T) set.getElementsArr());
		
		knoten = knoten.getNext();
		return (T) knoten.getElem();
		
		//return (T) knoten.getNext().getElem();
		
	}

}