package iterators;

import java.util.Iterator;

import setPackage.Set_2;


// E: DataType
// T: Elem<DataType>
public class Iterator_2<T, E> implements Iterator<T> {

	private Set_2<E> set;
	int pointer;
	
	
	public Iterator_2(Set_2<E> set) {
		this.set = set;
		this.pointer = -1;
	}
	
	@Override
	public boolean hasNext() {
		if(pointer < set.size()-1) {
			return true;
		}
		return false;
	}

	@Override
	public T next() {
		pointer++;
		System.out.println(pointer);
		//System.out.println((T) set.getNod());
		return (T) set.getNodeArray().get(pointer).getElem();
		
	}

}
