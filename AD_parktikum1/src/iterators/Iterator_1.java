package iterators;

import java.util.Iterator;

import setPackage.Set_1;


// E: DataType
// T: Elem<DataType>
public class Iterator_1<T, E> implements Iterator<T> {

	private Set_1<E> set;
	int pointer;
	
	
	public Iterator_1(Set_1<E> set) {
		this.set = set;
		this.pointer = 0;
	}
	
	@Override
	public boolean hasNext() {
		if(pointer < set.size()) {
			return true;
		}
		return false;
	}

	@Override
	public T next() {
		pointer++;
		//System.out.println(pointer);
		//System.out.println((T) set.getElementsArr());
		return (T) set.getElementsArr().get(pointer);
		
	}

}
