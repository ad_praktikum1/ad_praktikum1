package setPackage;

import model.*;

/**
 * Implementation of a Set
 * 
 * @author Johannes & Ronja
 *
 * @param <T> The set's Generic
 */
public interface Set_I<T> extends Iterable<Elem<T>> {
	
	/**
	 * adds a Value to the set. If already existing, nothing happens.
	 * 
	 * @param elem Element to add
	 */
	Position add(Elem elem);
	
	/**
	 * Deletes a value from the set.
	 * 
	 * @param position Position of the element to delete
	 */
	void delete(Position position);
	
	/**
	 * Deletes a value from the set
	 * 
	 * @param key Key of the element to delete.
	 */
	void delete(Key key);
	
	/**
	 * Finds the Position of an element by giving the element's key.
	 * 
	 * @param Key of the element
	 * @return Position of the element
	 */
	Position find(Key key);
	
	/**
	 * Deletes a value from the set
	 * 
	 * @param position Position of the element to delete
	 * @return Element with the given position
	 */
	Elem retrieve(Position position);
	
	/**
	 * Prints out all elements in the console
	 */
	void showall();
	
	/**
	 * Return the number of Elements in the set
	 * 
	 * @return number of elements
	 */
	int size();
	
	/**
	 * Unifies two sets
	 * 
	 * @param set1 set1
	 * @param set2 set2
	 * @return Unified set.
	 * 
	 */
	Set_I unify(Set_I<T> set1, Set_I<T> set2);

	/**
	 * returns the number of operations needed
	 * 
	 * @return number of operations as integer value.
	 * 
	 */
	long getTestCounter();

	/**
	 * resets the counter for number of operations
	 * 
	 */
	void resetTestCounter();
	
}
