
package setPackage;

import java.util.Iterator;

import iterators.Iterator_2;
import model.Elem;
import model.GenericArray;
import model.Key;
import model.Knoten;
import model.Position;

public class Set_2<T> implements Set_I<T> {
	GenericArray<Knoten<T>> nodeArray;
	int numNodes;
	long testCounter = 0L;
	
	public Set_2() {
		nodeArray = new GenericArray<Knoten<T>>(15);
		numNodes = 0;
	}



	@Override
	public Position add(Elem elem) {
		
		Knoten<T> newNode = new Knoten<T>(elem);
		
		//check size of array is sufficient
		if (numNodes+1 >= nodeArray.getLength()){
			GenericArray<Knoten<T>> cacheGenericArray = new GenericArray<>(nodeArray.getLength() * 2 + 1);
			
			//copying old values into new array
			for(int i = 0; i < nodeArray.getLength(); i++) {
				cacheGenericArray.set(i, nodeArray.get(i));
			}
			
			nodeArray = cacheGenericArray;
			System.out.println("Array verg��ert auf" + nodeArray.getLength());
		}
		
		if (numNodes == 0) {
			newNode.setPrevIndex(numNodes);
			newNode.setPosition(new Position(true, numNodes, this, newNode));
			nodeArray.set(numNodes, newNode);
			numNodes++;
			return newNode.getPosition();
		} else {
			if (find(newNode.getElem().getKey()) != null) {
			Position temp = find(newNode.getElem().getKey());
			//temp = temp.getKnoten().getPosition();
				return temp;
			} else {
				newNode.setPrevIndex(numNodes-1);
				nodeArray.get(numNodes-1).setNextIndex(numNodes);
				newNode.setPosition(new Position(true, numNodes, this, newNode));
				nodeArray.set(numNodes, newNode);
				numNodes++;
				return newNode.getPosition();
			}
			
		}
	}

	@Override
	public void delete(Position position) {
		if(position.isValid()) {
			deleteUnchecked(position.getPointer());
			numNodes--;
			testCounter+=5L;
		}
	}


	
	@Override
	public void delete(Key key) {
		Position pos;
		pos = find(key);
		deleteUnchecked(pos.getPointer());
		numNodes--;
		testCounter+=6L;
	}

	private void deleteUnchecked(int index) {
		if ( index == nodeArray.getLength()-1){
			nodeArray.set(index, null);
			testCounter+=3L;
		} else{
			for(int i = index; i < numNodes; i++ ) {
				nodeArray.set(i, nodeArray.get(nodeArray.get(i).getNextIndex()));
				if(i>0) {
					nodeArray.get(i).setPrevIndex(nodeArray.get(i).getPrevIndex()-1);
					nodeArray.get(i-1).setNextIndex(i);
					testCounter+=13L;
				} else {
					nodeArray.get(i).setPrevIndex(i);
					testCounter+=9L;
				}
				nodeArray.get(i).getPosition().setPointer(nodeArray.get(i).getPosition().getPointer());
				testCounter+=1L;
			} 
		}
	}
	
/*	@Override
	public Position find(Key key) {
		for(int i = 0; i <= numNodes; i++ ) {
			if(key.equals(nodeArray.get(i).getElem().getKey())) {
				return new Position(i, this);
			}
		}
		
		return null;
	} */
	
	@Override
	public Position find(Key key) {
		T data = null;
		int i = 0;
		Knoten<T> tempNode = new Knoten<T>(new Elem<T>(key, data));
		nodeArray.set(nodeArray.getLength()-1, tempNode);
		Knoten<T> temp = nodeArray.get(i);
		testCounter+=9L;
		while (nodeArray.get(temp.getPosition().getPointer()).getElem().getKey().getKey() != key.getKey()) {
			++i;
			if ( nodeArray.get(i) != null) {
			temp = nodeArray.get(i);
			testCounter+=12L;
			} else {
				deleteUnchecked(nodeArray.getLength()-1);
				testCounter+=13L;
				return null;
			}

		}
		deleteUnchecked(nodeArray.getLength()-1);
		testCounter+=2L;
		return temp.getPosition();
	} 

	@Override
	public Elem retrieve(Position position) {
		if(position.isValid()) {
			return nodeArray.get(position.getPointer()).getElem();
		}
		return null;
	}

	@Override
	public void showall() {
		for(int i = 0; i < numNodes; i++) {
			System.out.println(nodeArray.get(i).getElem());
		}
		
	}

	@Override
	public int size() {
		//System.out.println(numNodes);
		return numNodes;
	}


	@Override
	public Set_I unify(Set_I<T> set1, Set_I<T> set2) {
		for (Elem<T> elem : set1) {
			set2.add(elem);
		}

		return set2;
	} 

	public GenericArray<Knoten<T>> getNodeArray() {
		return nodeArray;
	}

	@Override
	public Iterator<Elem<T>> iterator() {
		//return null;
		return new Iterator_2<Elem<T>, T>(this);

	}

	public long getTestCounter() {
		return testCounter;
	}
	
	public void resetTestCounter() {
		this.testCounter = 0L;
	}

/*	@Override
	public Set_I unify(Set_I<T> set1, Set_I<T> set2) {
		// TODO Auto-generated method stub
		return null;
	} */

}

