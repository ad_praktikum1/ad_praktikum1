package setPackage;

import java.util.Iterator;

import iterators.Iterator_1;
import model.Elem;
import model.GenericArray;
import model.Key;
import model.Position;

public class Set_1<T> implements Set_I<T> {

	GenericArray<Elem<T>> elementsArr;
	int elementsCounter;
	long testCounter = 0L;

	public Set_1() {
		elementsArr = new GenericArray<Elem<T>>(15);
		elementsCounter = 0;
	}

	@Override
	public Position add(Elem elem) {
		if (elementsCounter + 1 >= elementsArr.getLength()) {// making array
																// bigger in
																// case it's too
																// small

			GenericArray<Elem<T>> cacheGenericArray = new GenericArray<>(elementsArr.getLength() * 2 + 1);

			// copying of old values into new array
			for (int i = 1; i < elementsArr.getLength(); i++) {
				cacheGenericArray.set(i, elementsArr.get(i));
			}

			elementsArr = cacheGenericArray;
			System.out.println("Array verg��ert auf" + elementsArr.getLength());
		}

		for (Elem oldElem : this) {
			if (oldElem.equals(elem)) {
				return null;
			}
		}

		elementsArr.set(elementsCounter + 1, elem);
		elementsCounter++;
		return new Position(elementsCounter, this);

	}

	@Override
	public void delete(Position position) {
		if (position != null && position.isValid()) {
			deleteUnchecked(position.getPointer());
			testCounter+=4L;
		}
	}

	@Override
	public void delete(Key key) {
		for (int i = 1; i <= size(); i++) {
			if (elementsArr.get(i).getKey().equals(key)) {
				deleteUnchecked(i);
				testCounter+=6L;
			}
		}
	}

	private void deleteUnchecked(int index) {
		for (int i = index; i <= elementsCounter; i++) {
			elementsArr.set(i, elementsArr.get(i + 1));
			testCounter+=3L;
		}
		elementsCounter--;
		testCounter+=1L;
	}

	@Override
	public Position find(Key key) {

		for (int i = 1; i <= elementsCounter; i++) {
			if (key.equals(elementsArr.get(i).getKey())) {
				return new Position(i, this);
			}
		}

		return null;
	}

	@Override
	public Elem retrieve(Position position) {
		if (position.isValid()) {
			return elementsArr.get(position.getPointer());
		}
		return null;
	}

	@Override
	public void showall() {
		for (int i = 0; i <= elementsCounter; i++) {
			System.out.println(elementsArr.get(i));
		}
	}

	@Override
	public int size() {
		return elementsCounter;
	}

	@Override
	public Iterator<Elem<T>> iterator() {
		return new Iterator_1<Elem<T>, T>(this);
	}

	public GenericArray<Elem<T>> getElementsArr() {
		return elementsArr;
	}

	@Override
	public Set_I unify(Set_I<T> set1, Set_I<T> set2) {
		for (Elem<T> elem : set1) {
			set2.add(elem);
		}

		return set2;

	}

	@Override
	public long getTestCounter() {
		return testCounter;
	}

	@Override
	public void resetTestCounter() {
		this.testCounter = 0;		
	}

}
