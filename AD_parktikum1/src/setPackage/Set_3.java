package setPackage;

import java.util.Iterator;
import iterators.Iterator_3;
import model.Elem;
import model.Key;
import model.Knoten;
import model.Position;

public class Set_3<T> implements Set_I<T> {
	Knoten<T> head;
	Knoten<T> tail;
	int size;
	Position pos;
	T data;
	long testCounter = 0L;

	public Set_3() {
		size = 0;
		head = new Knoten<T>(null);
		tail = new Knoten<T>(null);
		head.setNext(tail);
		tail.setNext(head);
		pos = new Position(0, this);
		head.setPosition(new Position(false, 0, this, head));
		tail.setPosition(new Position(false, 1, this, tail));

	}


	@Override
	public Position add(Elem elem) {

		Knoten<T> neu = new Knoten<T>(elem);
		if (size == 0) {
			size++;
			neu.setNext(tail);
			head.setNext(neu);
			tail.setNext(neu);
			neu.setPosition(new Position(true, size, this, neu));
			tail.getPosition().setPointer(tail.getPosition().getPointer() + 1);
			return neu.getPosition();
		} else {
			Position temp = find(elem.getKey());
			temp = temp.getKnoten().getNext().getPosition();
			if (!temp.isValid()) {
				Knoten<T> letzter = tail.getNext();
				letzter.setNext(neu);
				neu.setNext(tail);
				tail.setNext(neu);
				neu.setPosition(new Position(true, (this.size + 1), this, neu));
				tail.getPosition().setPointer(tail.getPosition().getPointer() + 1);
				size++;
				return neu.getPosition();
			} else {
				return temp;
			}
		}

	}
/*
	@Override
	public void delete(Position position) {
		if (position.getPointer() < 1 || position.getPointer() > this.size) return;
		Knoten<T> temp = head;
		while (temp.getNext().getPosition().getPointer() != position.getPointer()) {
			temp = temp.getNext();
		}
		temp.setNext(temp.getNext().getNext());
		size--;
		countPos();
	}
*/	
	
	
@Override
public void delete(Position position) {

//if (position.getKnoten().getNext().getPosition().getPointer() < 1 || position.getKnoten().getNext().getPosition().getPointer() > this.size) return;
	if(position.getKnoten().getNext().getPosition().isValid()) {
		System.out.println("if is true");
		
	position.getKnoten().setNext(position.getKnoten().getNext().getNext());
	size--;
	//showall();
	
	countPos();
	testCounter+=11L;
	}
	//showall();
}


	
	@Override
	public void delete(Key key) {
		pos = find(key);
		testCounter+=2L;
		if (	pos.getKnoten().getNext().getPosition().isValid()	) {
			pos.getKnoten().setNext(pos.getKnoten().getNext().getNext());
			this.size--;
			countPos();	
			testCounter+=12L;
		}

	}

	
	@Override
	public Position find(Key key) {
		tail.setElem(new Elem(key, data));
		Knoten temp = head;
		testCounter+=3L;
		while (temp.getNext().getElem().getKey().getKey() != key.getKey()) {
			temp = temp.getNext();
			testCounter+=8L;
		}
		return temp.getPosition();
	}
	
	@Override
	public Elem retrieve(Position position) {
		if(position.getKnoten().getNext().getPosition().isValid()){
		return position.getKnoten().getNext().getElem();
		}
	else return null;
	}
	
	/*
	@Override
	public Elem retrieve(Position position) {
		position=position.getKnoten().getNext().getPosition();
		if(position.getPointer() >size){
			return null;
		}
		else{
		pos.setPointer(0);
		Knoten temp= head;
		while (pos.getPointer() !=position.getPointer()){
			temp=temp.getNext();
			pos.setPointer(pos.getPointer()+1);
		}
			return temp.getElem();
		}
	}
	*/
	@Override
	public void showall() {
		Knoten temp = head;
		for (int i = 0; i < size(); i++) {
			System.out.println(temp.getNext().getElem());
			temp = temp.getNext();
		}

	}

	@Override
	public int size() {
		return size;
	}

	public Knoten getTail() {
		return tail;
	}

	public void setTail(Knoten tail) {
		this.tail = tail;
	}

	public Knoten getHead() {
		return head;
	}

	public void setHead(Knoten head) {
		this.head = head;
	}

	public Set_I unify(Set_I<T> set1, Set_I<T> set2) {
		for (Elem<T> elem : set1) {
			set2.add(elem);
		}

		return set2;
	}

	public Iterator<Elem<T>> iterator() {
		return new Iterator_3<Elem<T>, T>(this);
	}

	public void countPos() {
		Knoten<T> temp = head;
		int i = 1;
		testCounter+=2L;
		while (!(temp.getNext().equals(this.getTail()))) {

			temp.getNext().getPosition().setPointer(i);;
			temp = temp.getNext();
			i++;
			testCounter+=10L;
		}

	}


	@Override
	public long getTestCounter() {
		return testCounter;
	}


	@Override
	public void resetTestCounter() {
		testCounter = 0L;
		
	}

}
