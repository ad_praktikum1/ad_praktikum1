package testPackage;

import org.junit.Before;
import org.junit.Test;

import model.Elem;
import model.Key;
import model.Position;
import setPackage.Set_3;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertSame;


public class TestFrame3 {



    private Elem e1;
    private Elem e2;
    private Elem e3;
    private Elem e4;
    private Elem e5;
    private Elem e6;
    private Elem e7;
    private Elem e8;
    private Elem e9;
    private Elem e10;
    private Elem e11;
    private Elem e12;
    private Elem e13;
    private Elem e14;
    private Elem e15;
    private Elem e16;


    private Set_3 set;

    @Before
    public void setUp() throws Exception {

        this.e1 = new Elem<>(new Key(1), "Hello World1");
        this.e2 = new Elem<>(new Key(2), "Hello World2");
        this.e3 = new Elem<>(new Key(3), "Hello World3");
        this.e4 = new Elem<>(new Key(4), "Hello World4");
        this.e5 = new Elem<>(new Key(5), "Hello World5");
        this.e6 = new Elem<>(new Key(6), "Hello World6");
        this.e7 = new Elem<>(new Key(7), "Hello World7");
        this.e8 = new Elem<>(new Key(8), "Hello World8");
        this.e9 = new Elem<>(new Key(9), "HHello World9");
        this.e10 = new Elem<>(new Key(10), "Hello World10");
        this.e11 = new Elem<>(new Key(11), "Hello World11");
        this.e12 = new Elem<>(new Key(12), "Hello World12");
        this.e13 = new Elem<>(new Key(13), "Hello World13");
        this.e14 = new Elem<>(new Key(14), "Hello World14");
        this.e15 = new Elem<>(new Key(15), "Hello World15");
        this.e16 = new Elem<>(new Key(16), "Hello World16");

        this.set = new Set_3();

    }

    @Test
    public void add() throws Exception {

        //twice added check
        Position pos1 = set.add(e1);
        Position pos2 = set.add(e1);
        assertSame("Element shoud not be added twice! Same reference expected!", pos1, pos2);
    }

    @Test
    public void addOutOfBoundTest() throws Exception {

        //tests if the list extends automatically
        set.add(e1);
        set.add(e2);
        set.add(e3);
        set.add(e4);
        set.add(e5);
        set.add(e6);
        set.add(e7);
        set.add(e8);
        set.add(e9);
        set.add(e10);
        set.add(e11);
        set.add(e12);
        set.add(e13);
        set.add(e14);
        set.add(e15);
        set.add(e16);
    }

    @Test
    public void deletePos() throws Exception {


        Position pos1 = set.add(e1);
        Position pos2 = set.add(e2);
        Position pos3 = set.add(e3);

        set.delete(pos1);
        set.delete(pos2);
        set.delete(pos3);

        set.delete(new Position(0,set));
        set.delete(new Position(10,set));
        set.delete(new Position(100,set));
        set.delete(new Position(10000,set));
        set.delete(new Position(-10,set));
    }

    @Test
    public void deleteKey() throws Exception {

        Position pos1 = set.add(e1); //key 1
        Position pos2 = set.add(e2); //key 2
        Position pos3 = set.add(e3); //key 3


    }

    @Test
    public void find() throws Exception {

        set.add(e1);
        Position pos = set.add(e2);
        set.add(e3);

        Position foundPos = set.find(new Key(2));
       foundPos=foundPos.getKnoten().getNext().getPosition();
        assertSame("Actual and found Position mismatch!", pos, foundPos);


    }

    @Test
    public void retrieve() throws Exception {

        set.add(e1); //Key 1
        set.add(e2); //Key 2
        set.add(e3); //Key 3

        Position foundPos = set.find(new Key(2));
//      foundPos=foundPos.getKnoten().getNext().getPosition();

        Elem foundElem = set.retrieve(foundPos);

        assertSame("Given and retrieved object missmatch!", e2, foundElem);

    }

    @Test
    public void showall() throws Exception {
        set.add(e1);
        set.add(e2);
        set.add(e3);
        set.add(e4);
        set.add(e5);
        set.add(e6);
        set.add(e7);
        set.add(e8);
        set.add(e9);
        set.add(e10);
        set.add(e11);
        set.add(e12);
        set.add(e13);
        set.add(e14);
        set.add(e15);
        set.add(e16);

        set.showall();
    }

    @Test
    public void size() throws Exception {

        assertEquals("Size has not given the correct size of the Set!",set.size(), 0);

        set.add(e1);
        set.add(e2);
        set.add(e3);
        assertEquals("Size has not given the correct size of the Set!",set.size(), 3);

        set.add(e1); //duplicate
        assertEquals("Size has not given the correct size of the Set!",set.size(), 3);

        set.delete(e1.getKey());
        assertEquals("Size has not given the correct size of the Set!",set.size(), 2);

        set.delete(e2.getKey());
        set.delete(e3.getKey());
        assertEquals("Size has not given the correct size of the Set!",set.size(), 0);

    }





}
