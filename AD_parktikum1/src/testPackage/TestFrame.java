
package testPackage;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import model.Elem;
import model.Key;
import setPackage.Set_1;
import setPackage.Set_2;
import setPackage.Set_3;
import setPackage.Set_I;

public class TestFrame {

	Key basicKey = new Key(1);
	Elem<String> basicElem = new Elem<String>(basicKey, "Test");
	int testNumber = 122;
	
	@Test
	public void testAll() {
		testOne(new Set_1<>());
		testOne(new Set_2<>());
		//testOne(new Set_3<>());
	}
	
	private <T> void testOne(Set_I set) {
		
		basicAddSizeMatch(set);
		basicFindRetrieveTest(set);
		basicTestForEach(set);
		basicTestAddEquals(set);
		emptySet(set);
		fillSet(testNumber, set);
		advancedforeach(set);
		advancedFindTest(testNumber/2, set);
		testUnify(set, set);
		
		
		
	}
	
	
	private void basicAddSizeMatch(Set_I set) {
		assertEquals(0, set.size());
		set.add(basicElem);
		assertEquals(1, set.size());
	}
	
	private void basicFindRetrieveTest(Set_I set) {
		assertEquals(basicElem.getData(), set.retrieve(set.find(basicKey)).getData());
	}
	
	private <T> void basicTestForEach(Set_I<T> set) {
		int counter = 0;
		set.showall();
		for(Elem<T> elem: set) {

			assertEquals(basicElem, elem);
			counter++;
		}
		
		assertEquals(1, counter);
	}
	
	private void basicTestAddEquals(Set_I set) {
		set.add(basicElem);
		assertEquals(1, set.size());
	}
	
	private void emptySet(Set_I set) {
		set.delete(basicKey);
		assertEquals(set.size(), 0);
	}
	
	private void fillSet(int number, Set_I set) {
		List<Elem> list = new ArrayList<>();
		for(int i = 1; i<= number; i++) {
			list.add(new Elem(new Key(i), new String("test" + i)));
		}
		for(Elem elem: list) {
			set.add(elem);
		}
		assertEquals(set.size(), number);
	}
	
	
	private <String> void advancedforeach(Set_I<String> set) {
		int counter = 0;
		
		for(Elem<String> elem: set) {
			counter++;
		}
		System.out.println(counter+" counter");
		assertEquals(testNumber, counter);
	}
	
	private void advancedFindTest(int keyToFind, Set_I set) {
		set.delete(set.find(new Key(keyToFind)));
		assertEquals(testNumber-1, set.size());
	}
	
	private void testUnify(Set_I set1, Set_I set2) {
		Set_I uniSet = set1.unify(set1, set2);
		assertEquals(testNumber-1, uniSet.size());
	}
	
	
}

