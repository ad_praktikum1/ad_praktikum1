package testPackage;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import model.Elem;
import model.Key;
import model.Position;
import setPackage.Set_1;
import setPackage.Set_2;
import setPackage.Set_3;
import setPackage.Set_I;

public class TestFrameQuantity {
	List<Elem>list10;
	List<Elem>list100;
	List<Elem>list1000;
	List<Elem>list10000;
	List<Elem>list100000;
	Position pos;
	
	@Test
	public void testAll() {
		//testOne(new Set_1<>());
		testOne(new Set_2<>());
		//testOne(new Set_3<>());
	}
	private <T> void testOne(Set_I set) {
		testSetup();
		this.pos = new Position(false, 0, set);
		deleteKey(list10, set);
		deletePos(list10, set);
		deleteKey(list100, set);
		deletePos(list100, set);
		deleteKey(list1000, set);
		deletePos(list1000, set);
		deleteKey(list10000, set);
		deletePos(list10000, set);
		deleteKey(list100000, set);
		deletePos(list100000, set);
	}
	
	private void testSetup() {
		this.list10 = generateList(10);
		this.list100 = generateList(100);
		this.list1000 = generateList(1000);
		this.list10000 = generateList(10000);
		this.list100000 = generateList(100000);
	}
	private List<Elem> generateList(int number) {
		List<Elem> list = new ArrayList<>();
		for(int i = 1; i<= number; i++) {
			list.add(new Elem(new Key(i), new String("test" + i)));
		}
		return list;
	}
	
	private <T> void deleteKey(List<Elem>list, Set_I<T> set) {
		for(Elem elem: list) {
			set.add(elem);
		}
		for(Elem elem: list) {
			set.delete(elem.getKey());
		}
		System.out.println("DeleteKey for:"+list.size()+" elements, needs "+set.getTestCounter());
		set.resetTestCounter();
	}
	
	private <T> void deletePos(List<Elem>list, Set_I<T> set) {
		for(Elem elem: list) {
			set.add(elem);
		}
		for(int i = set.size(); i > 0; i--) {
			pos.setPointer(i);
			set.delete(pos);
		}
		System.out.println("DeletePos for:"+list.size()+" elements, needs "+set.getTestCounter());
		set.resetTestCounter();
	}
}
